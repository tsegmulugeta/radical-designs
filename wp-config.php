<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Sn1RXDHCxP_r|,&iv:v>saHGQ221BO?aKR?uD_?IKI&jdJY,>|WUo/#?YcMR*ZwW');
define('SECURE_AUTH_KEY',  'L}l0=S${_S&!3CPSG?(k964aiTMforCOMh-4Y{Igmfgy/*;Cxiwv!@z^B$i:hQIL');
define('LOGGED_IN_KEY',    'rXnG$@A!+;.k`/./+|#45vE%Od/_SVs/yni%G?:F-XNX|xD0]/%|RbUs%6DVe!xM');
define('NONCE_KEY',        '3-/h5VzQ!7m3hSjzwkdrY_do=<1J8o{Mstc70qXPy~*En|(2kNgK4 ~TuTTFr1d-');
define('AUTH_SALT',        '71VY]>Q,OZPXe<GF:$$tbb=8m[ff?IWu7xcolUz70C/&a-2*.q,zZcQ~_#!-5h<B');
define('SECURE_AUTH_SALT', '@rv2OffH2{<(!=+Ym2vRG:)@*g]lQz7>I$5(88&tko/ovE0RhuH;I^.]?G:i&uNH');
define('LOGGED_IN_SALT',   '[XcHTCKxl>1R@%6F?~}cyW01pxJ*aoFPl~YQBSpI0_{r&j|0zcB[aB:qn1<v<N6@');
define('NONCE_SALT',       'lZ}nlO>@YVUBG3O;h(W]Z9O`tXY--`U& VLg@0-m3Z<9#|c~W1-7&F_4tR!*QFS0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
